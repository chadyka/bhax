#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
    // nyitunk egy új ablakot a programunknak
    WINDOW *ablak;
    ablak = initscr();

    // x és y koordináták fogják megadni a kurzor pozícióját
    int x = 0;
    int y = 0;

    // ezek a változók fogják meghatározni a kurzor következő helyét
    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    // getmaxyx() függvény megadja az ablak max szélességét és magasságát my és mx változókba
    // szétbontva lehetne my = getmaxy(ablak); mx = getmaxx(ablak);
    getmaxyx(ablak, my, mx);
    
    for (;;)
    {

        // a kurzor ami (x,y) koordinátán áll kiír ""-ben lévő stringet 
        mvprintw(y, x, "");

        // az ablakra való kiírás nem jelenik meg amíg nem frissítjük a stdsrc-t
        // ebben az esetben wrefresh(stdscr) az alapértelmezett ami ekvivalens a refresh() függvénnyel
        refresh();

        // a program álljon le a usleep() paraméterének megadott microszekundumig
        usleep(50000);

        // inkrementáljuk a koordinátáinkat, hogy egyenletes 45 fokos szögben haladjanak tovább
        x = x + xnov;
        y = y + ynov;

        if (x >= mx - 1)
        { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        if (x <= 0)
        { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        if (y <= 0)
        { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        if (y >= my - 1)
        { // elerte-e az aljat?
            ynov = ynov * -1;
        }
    }

    return 0;
}