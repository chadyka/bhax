#include <stdio.h>

int main()
{
    // legyen n a számunk aminek az utolsó bitjei jelenleg ...0001
    int n = 1;
    // mivel 1 bit-ről kezdünk, a méret is egyelőre 1 bit
    int meret = 1;

    /*
    shifteljük az n utolsó bitjét egyel balra így rendre az utolsó bitek:
    ...0010; ...0100; ...1000; és így tovább amíg a rendelkezésre álló
    bitekből ki nem futunk és a számunk bitjei csupa 0-k lesznek emiatt
    pedig a while(0) miatt kilépünk a ciklusból
    */
    while (n <<= 1)
    {
        // minden iterációval mérjük a méretét n-nek vagyis hogy hol tart a shiftelt 1-es
        ++meret;
    }

    // az int méretét az értelmezés megkönnyebítéséért byte-ban írjuk ki bitek helyett
    printf("Az int mérete ezen a gépen: %d byte\n", meret / 8);

    return 0;
}