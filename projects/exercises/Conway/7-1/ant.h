#ifndef ANT_H
#define ANT_H

class Ant
{

public:
    int x;
    int y;
    int dir;

    Ant(int x, int y): x(x), y(y) {

        // random számot generálunk ami a
        // modulus miatt 0 és 7 közé fog esni
        dir = qrand() % 8;

    }

};

// a hangyákat egy hangya objektumokból álló vektorban fogjuk tárolni
typedef std::vector<Ant> Ants;

#endif
