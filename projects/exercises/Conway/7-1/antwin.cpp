#include "antwin.h"
#include <QDebug>

// A Hangya Qt ablak konstruktora
AntWin::AntWin ( int width, int height, int delay, int numAnts,
                 int pheromone, int nbhPheromon, int evaporation, int cellDef,
                 int min, int max, int cellAntMax, QWidget *parent ) : QMainWindow ( parent )
{
    // az ablak neve legyen Ant simulation

    setWindowTitle ( "Ant Simulation" );

    this->width = width;
    this->height = height;
    this->max = max;
    this->min = min;

    cellWidth = 6;
    cellHeight = 6;

    // megadjuk az ablak kívánt méretét
    setFixedSize ( QSize ( width*cellWidth, height*cellHeight ) );

    // elkészítjük a rácsot amiden a hangyáink mozogni fognak
    grids = new int**[2];
    grids[0] = new int*[height];
    for ( int i=0; i<height; ++i ) {
        grids[0][i] = new int [width];
    }
    grids[1] = new int*[height];
    for ( int i=0; i<height; ++i ) {
        grids[1][i] = new int [width];
    }

    gridIdx = 0;
    grid = grids[gridIdx];

    for ( int i=0; i<height; ++i )
        for ( int j=0; j<width; ++j ) {
            grid[i][j] = cellDef;
        }

    // Egy új Ants osztályú vektort példányosítunk
    ants = new Ants();

    // AntThread osztályt példányosítunk
    antThread = new AntThread ( ants, grids, width, height, delay, numAnts, pheromone,
                                nbhPheromon, evaporation, min, max, cellAntMax);

    // Signal leadását készítjük elő, ami a Qt keretrendszer különlegessége
    // Akkor adunk le szignált amikor az objektum állapota úgy változik,
    // hogy az a kliens vagy a tuajdonos szempontjából fontos vagy érdekes
    // Amikor egy szignált kiadunk a hozzá kötött Slot egyből le fog futni

    // Ebben az esetben a step függvényünket "huzalozzuk be", így
    // akárhányszor változik az antThread objektum, meg fog hívódni
    // a step() ami megvalósítja azt, hogy a hangya
    // helyet váltson, frissíti a rácsot
    connect ( antThread, SIGNAL ( step ( int) ),
              this, SLOT ( step ( int) ) );

    // elindítjuk a Qt alkalmazást
    antThread->start();

}

// Qt paintEvent megvalósítása
void AntWin::paintEvent ( QPaintEvent* )
{
    // QPainter objektumot példányosítunk
    QPainter qpainter ( this );

    // átvesszük a rácsot
    grid = grids[gridIdx];

    // végiglépkedünk a rácson
    for ( int i=0; i<height; ++i ) {
        for ( int j=0; j<width; ++j ) {

            double rel = 255.0/max;

            // kitöltjük a cellákat fehérrel
            qpainter.fillRect ( j*cellWidth, i*cellHeight,
                                cellWidth, cellHeight,
                                QColor ( 255 - grid[i][j]*rel,
                                         255,
                                         255 - grid[i][j]*rel) );

            if ( grid[i][j] != min )
            {
                // megadjuk a toll színét
                qpainter.setPen (
                    QPen (
                        QColor ( 255 - grid[i][j]*rel,
                                 255 - grid[i][j]*rel, 255),
                        1 )
                );

                // megrajzoljuk a cellát
                qpainter.drawRect ( j*cellWidth, i*cellHeight,
                                    cellWidth, cellHeight );
            }


            // megadjuk a toll színét (fekete)
            qpainter.setPen (
                QPen (
                    QColor (0,0,0 ),
                    1 )
            );

            // megrajzoljuk a cellákat
            qpainter.drawRect ( j*cellWidth, i*cellHeight,
                                cellWidth, cellHeight );

        }
    }

    // foreach ciklussal végigmegyünk az ants vektor elemein
    for ( auto h: *ants) {

        // megadjuk a toll színét
        qpainter.setPen ( QPen ( Qt::black, 1 ) );

        // megrajzoljuk a hangyát
        qpainter.drawRect ( h.x*cellWidth+1, h.y*cellHeight+1,
                            cellWidth-2, cellHeight-2 );

    }

    // befejeztük a rajzolást
    qpainter.end();
}

// Hangya ablak destruktora
AntWin::~AntWin()
{
    delete antThread;

    for ( int i=0; i<height; ++i ) {
        delete[] grids[0][i];
        delete[] grids[1][i];
    }

    delete[] grids[0];
    delete[] grids[1];
    delete[] grids;

    delete ants;
}

// frissíti a rácsot, a következő pillanatra lép
void AntWin::step ( const int &gridIdx )
{

    this->gridIdx = gridIdx;
    update();
}
