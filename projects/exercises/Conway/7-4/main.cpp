#include <QApplication>
#include <QTextStream>
#include <QtWidgets>
#include "BrainBWin.h"

int main ( int argc, char **argv )
{
        // Inicializáljuk a Qt alkalmazást,
        // átvesszük a parancssori argumentumokat
        QApplication app ( argc, argv );

        // QTextStream osztály könnyíti a
        // log üzenetek kiírását a keretrendszeren belül
        QTextStream qout ( stdout );
        qout.setCodec ( "UTF-8" );

        qout << "\n" << BrainBWin::appName << QString::fromUtf8 ( " Copyright (C) 2017, 2018 Norbert Bátfai" ) << endl;

        qout << "This program is free software: you can redistribute it and/or modify it under" << endl;
        qout << "the terms of the GNU General Public License as published by the Free Software" << endl;
        qout << "Foundation, either version 3 of the License, or (at your option) any later" << endl;
        qout << "version.\n" << endl;

        qout << "This program is distributed in the hope that it will be useful, but WITHOUT" << endl;
        qout << "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS" << endl;
        qout << "FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n" << endl;

        // QString::fromUtf8() fv-utf8 kódolású szöveget ír, ami az ékezetek miatt fontos
        qout << QString::fromUtf8 ( "Ez a program szabad szoftver; terjeszthető illetve módosítható a Free Software" ) << endl;
        qout << QString::fromUtf8 ( "Foundation által kiadott GNU General Public License dokumentumában leírtak;" ) << endl;
        qout << QString::fromUtf8 ( "akár a licenc 3-as, akár (tetszőleges) későbbi változata szerint.\n" ) << endl;

        qout << QString::fromUtf8 ( "Ez a program abban a reményben kerül közreadásra, hogy hasznos lesz, de minden" ) << endl;
        qout << QString::fromUtf8 ( "egyéb GARANCIA NÉLKÜL, az ELADHATÓSÁGRA vagy VALAMELY CÉLRA VALÓ" ) << endl;
        qout << QString::fromUtf8 ( "ALKALMAZHATÓSÁGRA való származtatott garanciát is beleértve. További" ) << endl;
        qout << QString::fromUtf8 ( "részleteket a GNU General Public License tartalmaz.\n" ) << endl;

        qout << "http://gnu.hu/gplv3.html" << endl;
        // hosszas fancy licensz és felhasználási szabályozás kiírás vége


        // Teljesképernyős módban indítjuk el az alkalmazást
        // és utólag kérjük el a képernyő méretét a grafikai megjelenítéshez
        QRect rect = QApplication::desktop()->availableGeometry();
        BrainBWin brainBWin ( rect.width(), rect.height() );
        brainBWin.setWindowState ( brainBWin.windowState() ^ Qt::WindowFullScreen );
        brainBWin.show();
        return app.exec();
}
