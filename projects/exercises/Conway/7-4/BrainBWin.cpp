#include "BrainBWin.h"

const QString BrainBWin::appName = "NEMESPOR BrainB Test";
const QString BrainBWin::appVersion = "6.0.3";

// Konstruktor
BrainBWin::BrainBWin ( int w, int h, QWidget *parent ) : QMainWindow ( parent )
{
        // statisztikák könyvtár nevének összefűzése
        statDir = appName + " " + appVersion + " - " + QDate::currentDate().toString() + QString::number ( QDateTime::currentMSecsSinceEpoch() );

        // Elindítjuk az alkalmazást
        brainBThread = new BrainBThread ( w, h - yshift );
        brainBThread->start();


        // szignál-slot összekötés
        connect ( brainBThread, SIGNAL ( heroesChanged ( QImage, int, int ) ),
                  this, SLOT ( updateHeroes ( QImage, int, int ) ) );

        connect ( brainBThread, SIGNAL ( endAndStats ( int ) ),
                  this, SLOT ( endAndStats ( int ) ) );

}

// program bezárását és a statisztika előállítását megvalósító fv
void BrainBWin::endAndStats ( const int &t )
{

        qDebug()  << "\n\n\n";
        qDebug()  << "Thank you for using " + appName;
        qDebug()  << "The result can be found in the directory " + statDir;
        qDebug()  << "\n\n\n";

        save ( t );
        close();
}

// A hősök állapotát változtatja és előkészíti az új hősök felvételét
void BrainBWin::updateHeroes ( const QImage &image, const int &x, const int &y )
{

        if ( start && !brainBThread->get_paused() ) {

                int dist = ( this->mouse_x - x ) * ( this->mouse_x - x ) + ( this->mouse_y - y ) * ( this->mouse_y - y );

                if ( dist > 121 ) {
                        ++nofLost;
                        nofFound = 0;
                        if ( nofLost > 12 ) {

                                if ( state == found && firstLost ) {
                                        found2lost.push_back ( brainBThread->get_bps() );
                                }

                                firstLost = true;

                                state = lost;
                                nofLost = 0;

                                brainBThread->decComp();
                        }
                } else {
                        ++nofFound;
                        nofLost = 0;
                        if ( nofFound > 12 ) {

                                if ( state == lost && firstLost ) {
                                        lost2found.push_back ( brainBThread->get_bps() );
                                }

                                state = found;
                                nofFound = 0;

                                brainBThread->incComp();
                        }

                }

        }
        pixmap = QPixmap::fromImage ( image );
        update();
}

// fenti függvényben megkapott pixel mappot színezi
void BrainBWin::paintEvent ( QPaintEvent * )
{
        if ( pixmap.isNull() ) {
                return;
        }

        QPainter qpainter ( this );

        // a teljes képernyőt sz x m -jét vesszük
        xs = ( qpainter.device()->width() - pixmap.width() ) /2;
        ys = ( qpainter.device()->height() - pixmap.height() +yshift ) /2;

        // megrajzolja a pixel mapot
        qpainter.drawPixmap ( xs, ys, pixmap );

        // kiírja a képrnyőre a szöveget
        qpainter.drawText ( 10, 20, "Press and hold the mouse button on the center of Samu Entropy" );

        // eltelt időt írjuk a bal felső sarokba
        int time = brainBThread->getT();
        int min, sec;
        millis2minsec ( time, min, sec );
        QString timestr = QString::number ( min ) + ":" + QString::number ( sec ) + "/10:0";
        qpainter.drawText ( 10, 40, timestr );

        // kiírja a jelenlegi bps értéket a bal felső sarokba
        int bps = brainBThread->get_bps();
        QString bpsstr = QString::number ( bps ) + " bps";
        qpainter.drawText ( 110, 40, bpsstr );

        // Paused állapotot kiírja a képernyőre
        if ( brainBThread->get_paused() ) {
                QString pausedstr = "PAUSED (" + QString::number ( brainBThread->get_nofPaused() ) + ")";

                qpainter.drawText ( 210, 40, pausedstr );
        }

        // befejezzük a rajzolást
        qpainter.end();
}

// egérgomb lenyomására indul a program
void BrainBWin::mousePressEvent ( QMouseEvent *event )
{

        brainBThread->set_paused ( false );

}

void BrainBWin::mouseReleaseEvent ( QMouseEvent *event )
{

        //brainBThread->set_paused(true);

}

// rögzítjük az egér helyzetét
void BrainBWin::mouseMoveEvent ( QMouseEvent *event )
{

        start = true;

        mouse_x = event->pos().x() -xs - 60;
        mouse_y = event->pos().y() - ys - 60;
}

// S, P, Q és escape billentyűk funkciói
// S ment | P megállítja a progit | Q vagy ESCAPE kilép
void BrainBWin::keyPressEvent ( QKeyEvent *event )
{

        if ( event->key() == Qt::Key_S ) {
                save ( brainBThread->getT() );
        } else if ( event->key() == Qt::Key_P ) {
                brainBThread->pause();
        } else if ( event->key() == Qt::Key_Q ||  event->key() == Qt::Key_Escape ) {
                close();
        }

}
// dekonstruktor
BrainBWin::~BrainBWin()
{

}
