public class exorTitkosito {

    // a class-unk paraméterei ebben a példában sorra: kulcs, System.in, System.out
    public exorTitkosito(String kulcsSzöveg, java.io.InputStream bejövőCsatorna, java.io.OutputStream kimenőCsatorna)
            throws java.io.IOException {

        // byte adattípusú tömbbe olvassuk a kulcsunkat ezzel tényleges byte formájában
        // tároljuk a kulcs karaktereit
        byte[] kulcs = kulcsSzöveg.getBytes();
        // 256 byte méretű byte tömbbe mentjük a kódolandó szöveget majd
        byte[] buffer = new byte[256];

        // a kulcsIndex segít a kulcs karakterei közt lépkedni
        // az olvasottBajtok pedig a System.in-en beolvasott bájtok száma lesz
        int kulcsIndex = 0;
        int olvasottBájtok = 0;

        // a read() metódus beolvassa a buffer-be a System.in-ről a szöveget
        // majd visszaadja a beolvasott bájtok számát (EOF esetén -1 et ad vissza és
        // kilép a ciklusból)
        while ((olvasottBájtok = bejövőCsatorna.read(buffer)) != -1) {

            // végigmegyünk a beolvasott bájtokon
            for (int i = 0; i < olvasottBájtok; ++i) {

                // bájtonként EXOR-ozzuk a jelenlegi karakter bájtját a
                // kulcs adott karakterének bájtjával
                // minden művelet után beolvasunk egy új bájtot
                // és a kulcs soron következő elemével elvégezzük azt
                buffer[i] = (byte) (buffer[i] ^ kulcs[kulcsIndex]);
                kulcsIndex = (kulcsIndex + 1) % kulcs.length;

            }
            // az eredményt kiíratjuk a kimenetre, ezesetben System.out
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);

        }

    }

    public static void main(String[] args) {

        // IOexceptiont try/catch blokkal kezeljük le
        try {
            // meghívjuk a titkosítónkat egy új objekttumként
            // ami örökli a class tulajdonságait az alább
            // látható paraméterlistával
            // (első parancssori argumentum, Parancsablak bemenete, Parancsablak kimenete)
            new exorTitkosito(args[0], System.in, System.out);

        } catch (java.io.IOException e) {
            // elkapjuk az esetleges IOExceptiont és kiírjuk a hiba
            // okát stacktrace formában (hívási sorrendben a hiba gyökerétől)
            e.printStackTrace();

        }

    }
}
