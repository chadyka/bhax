#include <stdio.h>
#include <unistd.h>
#include <string.h>

// a kulcs és a buffer maximális méretét #define kulcsszóval nevesített konstansban tároljuk
#define MAX_KULCS 100
#define BUFFER_MERET 256

// a main-ünk parancssori argumentumokon keresztül fogja kapni az értékeit
int main(int argc, char **argv)
{
    // karaktertömböket hozunk létre a kulcsnak és buffernek
    char kulcs[MAX_KULCS];
    char buffer[BUFFER_MERET];

    // a kulcs index-el fogunk haladni a kulcs elemein
    int kulcs_index = 0;
    // az olvasott bajtok számát tárolja majd
    int olvasott_bajtok = 0;

    // A kulcs mérete az első arugmentumból beolvasott karakterlánc hossza
    int kulcs_meret = strlen(argv[1]);
    // a kulcs tömbbe belemásoljuk az első argumentumból beolvasott karakterláncot
    strncpy(kulcs, argv[1], MAX_KULCS);

    // a read() beolvassa a bufferünkbe a szöveget (256-ot) majd EOF jelnél kilép a ciklusból
    while ((olvasott_bajtok = read(0, (void *)buffer, BUFFER_MERET)))
    {
        //egy for ciiklusal végigmegyünk a beolvasott szövegen
        for (int i = 0; i < olvasott_bajtok; ++i)
        {
            //minden karaktert a kulcsunk aktuális karakterével EXOR-ozunk,
            //majd a kulcs indexét folyamatosan 0-(kulcs_meret)-ig növeljük
            // és amikor a kulcs_mérete-hez érünk 0 indextől kezdjük a maradékos osztás miatt
            buffer[i] = buffer[i] ^ kulcs[kulcs_index];
            kulcs_index = (kulcs_index + 1) % kulcs_meret;
        }

        //kiíratjuk a buffer tartalmát
        write(1, buffer, olvasott_bajtok);
    }

    return 0;
}
