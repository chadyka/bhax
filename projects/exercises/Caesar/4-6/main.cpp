#include <iostream>
#include "mlp.hpp"
#include <png++/png.hpp>

int main(int argc, char const *argv[])
{
    // parancssori argumentumból beolvasunk egy kép objektumot
    png::image <png::rgb_pixel> png_image(argv[1]);

    // lekérjük a kép objektumunk szélesség x magasságát vagyis a méretét
    int size = png_image.get_width() * png_image.get_height();

    // Perceptron osztályú változót példányosítunk
    Perceptron* p = new Perceptron ( 3, size, 256, 1 );

    // a képpontokra készítünk egy tömb mutatót
    double* image = new double[size];

    // végigmegyünk a kép képpontjain és pirosra "festjük" azokat
    for (int i = 0; i < png_image.get_width(); ++i)
        for (int j = 0; j < png_image.get_height(); ++j)
            image[i * png_image.get_width() + j] = png_image[i][j].red;

    // () operátor Perceptron osztályban
    // van definiálva és visszaadja a képhez
    // tartozó súlyértéket
    double value = (*p) (image);

    // kiíratjuk a súlyt
    std::cout << value << std::endl;

    // felszabadítjuk a használt memóriaterületeket
    delete p;
    delete [] image;

    return 0;
}
