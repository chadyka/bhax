#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define LOG_PERIOD 100

int main(int argc, char **argv)
{
    // Változókat deklarálunk a kulcsnak, a kulcsok karaktereinek olvasásához és
    // a kulcs számainak karakteres reprezentációjához
    int key;
    int keyindex = 0;
    char sKey[4];
    // valamint a beolvasott szövegnek is
    char buffer[1024];
    char newBuffer[1024];

    // read() függvénnyel meghatározzuk a beolvasott szöveg hosszát és változóban tároljuk
    int len = read(0, buffer, sizeof(buffer));
    // beállítunk egy treshold változót ami az egyezések száma alapján eldönti mit írjunk majd ki lehetséges találatnak a töréshez
    int threshold = len - 80;

    int logCnt = LOG_PERIOD - 1;

    // végigmegyünk az összes lehetséges kulcson
    for (key = 0; key < 1000; key++)
    {
        int i;
        int sum = 0;

        // a jelenlegi kulcsot sprintf() függvénnyel belemásoljuk karakterként az sKey karaktertömbbe
        sprintf(sKey, "%03d", key);

        // végigmegyünk az egész titkos szövegen
        for (i = 0; i < len; i++)
        {
            // EXOR-ozzuk a buffer első karakterét a kulcs első karakterével,
            // majd minden rákövetkező buffer karaktert a következő kulcs karakterrel
            char b = buffer[i] ^ sKey[keyindex];
            keyindex = (keyindex + 1) % 3;

            // az EXOR-ozott karaktert egy karaktertömbben tároljuk
            newBuffer[i] = b;

            // a megoldás szöveg, ezért ha az EXOR művelet után abc karaktert vagy szóközt kapunk
            // növeljük egyel a sum változónkat ami a találatok számát fogja tárolni
            if ((b >= 'a') && (b <= 'z') || (b >= 'A') && (b <= 'Z') || b == ' ')
            {
                sum++;
            }
        }

        // ha elég nagy a szövegkarakterek számának aránya, kiíratunk egy lehetséges megoldást
        if (sum > threshold)
        {
            write(1, "Key: ", 5);
            write(1, sKey, strlen(sKey));
            write(1, "\n", 1);
            write(1, newBuffer, len);
            write(1, "\n\n", 2);
        }

        // időnként kiíratjuk az aktuális kulcsot, így nyomon tudjuk követni hol jár a program a törésben
        if (++logCnt == LOG_PERIOD)
        {
            logCnt = 0;
            printf("Act. key: %s\n", sKey);
        }
    }
}