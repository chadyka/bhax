#include <stdlib.h>
#include <stdio.h>
int main(int argc, char const *argv[])
{
    int n = 5;
    double **tm;

    tm = (double **)malloc(n * sizeof(double *));

    for (int i = 0; i < n; ++i)
    {
        tm[i] = (double *)malloc((i + 1) * sizeof(double *));
    }

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
        {
            tm[i][j] = (i * i) + j;
        }
    }

    tm[3][0] = 42.0;
    (*(tm + 3))[1] = 43.0;
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
        {
            printf("%f, ", tm[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < n; ++i)
    {
        free(tm[i]);
    }
    free(tm);
}
