#include <iostream>

int main()
{
    double **tm = new double *[6];

    for (int i = 0; i < 6; ++i)
        tm[i] = new double[i + 1];

    for (int i = 0; i < 6; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * i + j;

    for (int i = 0; i < 6; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            std::cout << tm[i][j] << " ";

        std::cout << std::endl;
    }

    std::cout << "----------------------" << std::endl;

    tm[3][0] = 42;
    (*(tm + 3))[1] = 43;
    *(tm[3] + 2) = 44;
    *(*(tm + 3) + 3) = 45;

    for (int i = 0; i < 6; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            std::cout << tm[i][j] << " ";

        std::cout << std::endl;
    }

    for (int i = 0; i < 6; ++i)
    {
        delete[] tm[i];
    }

    delete[] tm;

    return 0;
}
