#include <stdio.h>
int f(int first, int second)
{
    return first * second;
}

int main()
{
    int a = 5;
    printf("%d %d", f(a, ++a), f(a, ++a));
    return 0;
}
