#include <signal.h>
#include <stdio.h>
int main(int argc, char const *argv[])
{
    int jelkezelo;
    // SIGINT = signal interrupt, SIG_IGN = signal ignored
    // a signal függvény első paramétere egy signal ami esetén a függvény második
    // paramétereként megadott függvény fog lefutni.
    if (signal(SIGINT, SIG_IGN) != SIG_IGN)
        signal(SIGINT, jelkezelo);
    return 0;
}
