#include <stdio.h>

int f(int *p)
{
    *p = 10;
    return *p;
}

int main()
{
    int a = 5;
    printf("%d %d", f(&a), a);
    return 0;
}
