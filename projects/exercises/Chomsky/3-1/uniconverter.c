#include <stdio.h>

int main()
{
    int dec;

    // bekérünk egy tetszőleges számot
    printf("Enter a decimal number: ");
    scanf("%d", &dec);

    printf("\nThe number in unary: ");

    if (dec == 0)
    {
        printf("  (zero)");
    }
    else
    {
        // a számot unárisan reprezentáljuk "|"-jel használatával
        for (int i = 0; i < dec; i++)
        {
            printf("|");
        }
    }

    printf("\n");

    return 0;
}