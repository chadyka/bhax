#include <iostream>
#include "Binrand.hpp"
#include "Binsearch.hpp"
#include "ZLWTree.hpp"



int main(int argc, char **argv, char **env)
{
	BinRandTree<int> bt;
	ZLWTree<char, '/', '0'> zt;

	bt << 8 << 9 << 5 << 2 << 7;
	bt.print();

	std::cout << std::endl;

	zt << '0' << '0' << '0' << '0' << '0';
	zt.print();

	// Copy constructor called when:
	ZLWTree<char, '/', '0'> zt2{zt};

	// Copy assignment called when:
	ZLWTree<char, '/', '0'> zt3;

	zt3 << '1' << '1' << '1' << '1' << '1';

	std::cout << "***" << std::endl;
	zt = zt3;
	std::cout << "***" << std::endl;

	// Move constructor called when:
	ZLWTree<char, '/', '0'> zt4 = std::move(zt2);

	// Move assignment called when:
	BinSearchTree<std::string> bts;

	bts << "alma"
		<< "korte"
		<< "banan"
		<< "korte";

	bts.print();

	return 0;
}
