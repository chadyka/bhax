#include "Binrand.hpp"

template <typename T>
class BinSearchTree : public BinRandTree<T>
{

public:
	BinSearchTree() {}
	BinSearchTree &operator<<(T value);
};

template <typename T>
BinSearchTree<T> &BinSearchTree<T>::operator<<(T value)
{
	if (!this->treep)
	{

		this->root = this->treep = new typename BinRandTree<T>::Node(value);
	}
	else if (this->treep->getValue() == value)
	{

		this->treep->incCount();
	}
	else if (this->treep->getValue() > value)
	{

		if (!this->treep->leftChild())
		{
			this->treep->leftChild(new typename BinRandTree<T>::Node(value));
		}
		else
		{

			this->treep = this->treep->leftChild();
			*this << value;
		}
	}
	else if (this->treep->getValue() < value)
	{

		if (!this->treep->rightChild())
		{
			this->treep->rightChild(new typename BinRandTree<T>::Node(value));
		}
		else
		{

			this->treep = this->treep->rightChild();
			*this << value;
		}
	}

	this->treep = this->root;

	return *this;
}