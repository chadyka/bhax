#include <iostream>
#include <random>
#include <functional>
#include <chrono>

// létrehozunk egy Unirand nevű classt
class Unirand
{

	// privát tagként definiálunk egy function class template-t random néven
	// a template megoldás így nem csak az általunk írt kódban,
	// hanem ebben az std névtérbeli function-ben is megjelenik
private:
	// a class definícióban kiderül, hogy a template <típus(argumentumok)>
	// felépítésben kéri a sablon deklarálását, így ez a function int-et fog
	// visszaadni
	std::function<int()> random;

	// publikus tagként megadjuk a konstruktort és a zárojel
	// operátor működését felülírjuk a class szemszögéből
public:
	// konstruktorban elkérünk egy seed, min, és max változót
	// amiből a bind segítségével megadunk egy eloszlást és
	// generátort ami megadja majd nekünk a random int-ünket
	// seed argumentumból a min és max zárt intervallumán belül
	// nagyon stílusos c++ 11 kódcsipet!!!
	Unirand(long seed, int min, int max) : random(
											   std::bind(
												   std::uniform_int_distribution<>(min, max),
												   std::default_random_engine(seed))) {}

	// megadjuk a classunk példányaira a () operátor működését
	// (adja vissza a konstruktáláskor generált random intet)
	int operator()() { return random(); }
};

// a kód egy különlegessége a sablonok használata ami a generatív
// programozás alappillére C++-ban

// ez a sablonosztály inicializáló utasítás azt mondja el nekünk,
// hogy az osztályban a T típusú tagok majd a class példányosítása
// során fog < > jeleken belül típust kapni és ezeket a tagokat már
// ezzel a típussal fogja értelmezni
template <typename T>
class BinRandTree
{
	// protected-ként deklaráljuk a BinRandTree osztályunk Node tagosztályát
protected:
	// létrehozzuk a Node osztályt
	class Node
	{
		// privátként deklaráljuk a változókat és a copy assign/constructort
		// és move assign/constructort
	private:
		// itt az első példa a template típus használatára
		// a változó típusa példányosításnál fog eldőlni
		T value;
		Node *left;
		Node *right;
		int count{0};

		Node(const Node &);
		Node &operator=(const Node &);
		Node(Node &&);
		Node &operator=(Node &&);

		// publikusként deklaráljuk a Node osztály tagfüggvényeit valamint a
		// konstruktort definiáljuk, és szintén deklaráljuk a publikusan elérhető
		// fa jelenlegi ágára mutató pointert, a gyökér pointert és a jelenlegi mélységet
	public:
		Node(T value) : value(value), left(nullptr), right(nullptr) {}
		T getValue() { return value; }
		Node *leftChild() { return left; }
		Node *rightChild() { return right; }
		void leftChild(Node *node) { left = node; }
		void rightChild(Node *node) { right = node; }
		int getCount() { return count; }
		void incCount() { ++count; }
	};

	Node *root;
	Node *treep;
	int depth{0};

public:
	// Constructor
	// definiáljuk a konstruktort, inicializáljuk az értékeit
	// és kiírunk egy "BT ctor" szöveget ami mutatja majd nekünk,
	// hogy mikor fut le a konstruktor
	BinRandTree(Node *root = nullptr, Node *treep = nullptr) : root(root), treep(treep)
	{
		std::cout << "BT ctor" << std::endl;
	}

	// Copy constructor
	// definiáljuk a másoló konstruktort a cp függvény segítségével
	// ez akkor fog lefutni, ha inicializálatlan objektumba másolunk
	BinRandTree(const BinRandTree &old)
	{
		// lekövetjük a másoló konstruktor futását
		std::cout << "BT copy ctor" << std::endl;

		// cp függvénynek átadjuk a meglévő fa gyökér és faág pointerét
		root = cp(old.root, old.treep);
	}

	// rekurzívan meghívja magát a cp függvény
	// ezzel felépítve az új fát csomópontonként
	Node *cp(Node *node, Node *treep)
	{
		Node *newNode = nullptr;

		if (node)
		{
			newNode = new Node(node->getValue());

			newNode->leftChild(cp(node->leftChild(), treep));
			newNode->rightChild(cp(node->rightChild(), treep));

			if (node == treep)
			{
				this->treep = newNode;
			}
		}

		return newNode;
	}

	// Copy assignment
	// a másoló konstruktoron alapszik a másóló értékadásunk,
	// hiszen először inicializálunk egy tmp nevű fát amit
	// azután swap segítségével megcseréljük az új fát
	// azzal amit felül akarunk írni a jobb oldali értékkel
	BinRandTree &operator=(const BinRandTree &old)
	{
		std::cout << "BT copy assign" << std::endl;

		BinRandTree tmp{old};
		std::swap(*this, tmp);
		return *this;
	}

	// Move constructor
	// a mozgató konstruktor fog lefutni ha meglévő entitásnak akarunk
	// értéket adni akkor így tudjuk implicit deklarálni, hogy miként
	// szeretnénk átmozgatni az entitás adatait
	BinRandTree(BinRandTree &&old)
	{
		std::cout << "BT move ctor" << std::endl;

		root = nullptr;
		// meghívjuk a mozgató értékadást 3x
		// _a, _b, _tmp
		*this = std::move(old);
	}

	// Move assignment
	// a mozgató értékadás fogja nekünk megadni, hogy
	// miként tesszük át az adatot az egyik fából a másikba
	BinRandTree &operator=(BinRandTree &&old)
	{
		std::cout << "BT move assign" << std::endl;

		std::swap(old.root, root);
		std::swap(old.treep, treep);

		return *this;
	}

	// BinRandTree destruktora
	// delTree() tagfüggvény végzi a fák felszabadítását a memóriából
	~BinRandTree()
	{
		std::cout << "BT destructor" << std::endl;
		delTree(root);
	}

	// további tagfüggvények deklarációja
	BinRandTree &operator<<(T value);
	void print() { print(root, std::cout); }
	void print(Node *node, std::ostream &os);
	void delTree(Node *node);

	// publikus random számot hozunk létre [0,2] intervallumban
	// egyenlő eloszlással, system_clock-ból számolva
	Unirand ur{std::chrono::system_clock::now().time_since_epoch().count(), 0, 2};

	// tulajdonképp egy getter az ur értékére
	// amit az Unirand beli () operátor számol
	int whereToPut()
	{
		return ur();
	}
};

// BinRandTree-ből származtatott bináris keresőfa
template <typename T>
class BinSearchTree : public BinRandTree<T>
{

public:
	// konstruktor
	BinSearchTree() {}
	// << operátorral bármilyen típusú jobboldali érték
	// megadható a template T alkalmazása miatt
	BinSearchTree &operator<<(T value);

	// destruktorra nincs szükség hiszen a
	// szülőosztály destruktora alkalmazható
};

// BinRandTree-ből származtatott ZLW-fa
// ez a sablon egy típust és két tetszőleges
// típusú változót fog kérni példányosításkor a fához
template <typename T, T vr, T v0>
class ZLWTree : public BinRandTree<T>
{

public:
	// konstruktor
	// tree pointert a gyökérre állítjuk
	ZLWTree() : BinRandTree<T>(new typename BinRandTree<T>::Node(vr))
	{
		this->treep = this->root;
	}

	// << operátorral bármilyen típusú jobboldali érték
	// megadható a template T alkalmazása miatt
	ZLWTree &operator<<(T value);
};

// BinRandTree << operátor definíció
template <typename T>
BinRandTree<T> &BinRandTree<T>::operator<<(T value)
{
	// elkérünk egy random számot
	int rnd = whereToPut();

	// ha treep nullpointer akkor
	// root-tól kezdjük építeni a fát
	if (!treep)
	{

		root = treep = new Node(value);
	}
	// ha a value a konstruktori akkor count++
	else if (treep->getValue() == value)
	{

		treep->incCount();
	}
	// ha a random szám = 0
	else if (!rnd)
	{

		treep = root;
		*this << value;
	}
	// 1-esnél megcsináljuk a baloldali csomópontot
	// ha nincs vagy lejjebb megyünk a fában
	else if (rnd == 1)
	{

		if (!treep->leftChild())
		{

			treep->leftChild(new Node(value));
		}
		else
		{

			treep = treep->leftChild();
			*this << value;
		}
	}
	// 2-esnél megcsináljuk a jobboldali csomópontot
	// ha nincs vagy lejjebb megyünk a fában
	else if (rnd == 2)
	{

		if (!treep->rightChild())
		{

			treep->rightChild(new Node(value));
		}
		else
		{

			treep = treep->rightChild();
			*this << value;
		}
	}

	return *this;
}

// BinSearchTree osztály << operátor működése
template <typename T>
BinSearchTree<T> &BinSearchTree<T>::operator<<(T value)
{
	// nullptr esetén új gyökér, új fa
	if (!this->treep)
	{

		this->root = this->treep = new typename BinRandTree<T>::Node(value);
	}
	// ha a jelenlegi és a fa ágában egyenlő a value count++
	else if (this->treep->getValue() == value)
	{

		this->treep->incCount();
	}
	// nagyobbat balra
	else if (this->treep->getValue() > value)
	{

		if (!this->treep->leftChild())
		{
			this->treep->leftChild(new typename BinRandTree<T>::Node(value));
		}
		else
		{

			this->treep = this->treep->leftChild();
			*this << value;
		}
	}
	// kisebbet jobbra
	else if (this->treep->getValue() < value)
	{

		if (!this->treep->rightChild())
		{
			this->treep->rightChild(new typename BinRandTree<T>::Node(value));
		}
		else
		{

			this->treep = this->treep->rightChild();
			*this << value;
		}
	}

	this->treep = this->root;

	return *this;
}

// ZLWTree << operátorának működése template alapján
template <typename T, T vr, T v0>
ZLWTree<T, vr, v0> &ZLWTree<T, vr, v0>::operator<<(T value)
{
	// sablonban definiált v0 érték akkor bal oldali ágban
	// létrehozunk ha nincs vagy megyünk lejjebb
	if (value == v0)
	{
		if (!this->treep->leftChild())
		{

			typename BinRandTree<T>::Node *node = new typename BinRandTree<T>::Node(value);
			this->treep->leftChild(node);
			this->treep = this->root;
		}
		else
		{

			this->treep = this->treep->leftChild();
		}
	}
	// egyébként jobb oldali ágban hozunk létre ha nincs
	// jobb oldali gyerek vagy megyünk lejjebb
	else
	{

		if (!this->treep->rightChild())
		{

			typename BinRandTree<T>::Node *node = new typename BinRandTree<T>::Node(value);
			this->treep->rightChild(node);
			this->treep = this->root;
		}
		else
		{

			this->treep = this->treep->rightChild();
		}
	}

	return *this;
}

// BinRandTree kiíratásának definíciója
// minden cout-ra megy mélységenként "---" pluszba
template <typename T>
void BinRandTree<T>::print(Node *node, std::ostream &os)
{
	if (node)
	{
		++depth;
		print(node->leftChild(), os);

		for (int i{0}; i < depth; ++i)
		{
			os << "---";
		}

		os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;

		print(node->rightChild(), os);
		--depth;
	}
}

// fa felszabadítása a memóriából
// rekurzívan töröljük a bal és jobb oldali csomópontokat
template <typename T>
void BinRandTree<T>::delTree(Node *node)
{
	if (node)
	{
		delTree(node->leftChild());
		delTree(node->rightChild());

		delete node;
	}
}

int main(int argc, char **argv, char **env)
{
	// int-ekre készült fa
	BinRandTree<int> bt;
	bt << 8 << 9 << 5 << 2 << 7;
	bt.print();

	std::cout << "***" << std::endl;

	// ZLWTree karakterekből 0 értékekkel
	ZLWTree<char, '/', '0'> zt;
	zt << '0' << '0' << '0' << '0' << '0';
	zt.print();

	// Copy constructor called when: (ZLWTree konstruktorral megadva)
	ZLWTree<char, '/', '0'> zt2{zt};

	std::cout << "***" << std::endl;

	// Copy assignment called when:
	ZLWTree<char, '/', '0'> zt3;
	zt3 << '1' << '1' << '0' << '0' << '1';
	zt3.print();

	std::cout << "***" << std::endl;
	zt = zt3;
	std::cout << "***" << std::endl;

	// Move constructor called when:
	ZLWTree<char, '/', '0'> zt4 = std::move(zt2);
	zt4.print();

	std::cout << "***" << std::endl;

	// BinSearchTree stringekkel
	BinSearchTree<std::string> bts;

	bts << "alma"
		<< "korte"
		<< "banan"
		<< "korte";

	bts.print();

	return 0;
}
