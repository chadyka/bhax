#include <iostream>

int fgv(){return 42;}

int main() {
	int n = 7;
	int &lvr{n};
	int &&rvr{8};

	std::cout << lvr << " " << rvr << std::endl;
	lvr = rvr;
	std::cout << lvr << " " << rvr << std::endl;

	int &&rvr2{fgv()};
	std::cout << lvr << " " << rvr << std::endl;

	// int &lvr2{43};
	// int &lvr2{fgv()};

	// int &&rvr3{n};

	return 0;
}
