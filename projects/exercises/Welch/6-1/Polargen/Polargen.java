// Létrehozzuk a Polargen osztályt
public class Polargen {
    // privátként hozzuk létre az attribútumokat
    private boolean nincsTarolt;
    private double tarolt;

    // a konstruktorban a bool attribútumot igazra állítjuk
    public Polargen() {
        nincsTarolt = true;
    }

    // innen hiányzik a c++ megvalósítás dekonstruktora, hiszen
    // Java nyelven a memória felszabadítását a Java garbage collector végzi

    /*
     * ha a nincsTarolt attríbútum igaz akkor előállítjuk a tárolandó számot random
     * számokból és matematikából ami nem programozás tehát ugorjuk
     */
    public double kovetkezo() {
        if (nincsTarolt) {
            double u1, u2, v1, v2, w;
            do {
                u1 = Math.random();
                u2 = Math.random();
                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;
                w = v1 * v1 + v2 * v2;
            } while (w > 1);

            double r = Math.sqrt((-2 * Math.log(w)) / w);

            // eltároljuk az r*v2 számot a tarolt változóban, így a
            // következő iterációban ezt fogjuk kiíratni a console-ra
            tarolt = r * v2;
            // a bool változót hamisra állítjuk
            nincsTarolt = !nincsTarolt;

            // visszaadjuk az r * v1 számot
            return r * v1;
        } else {
            // ha a nincsTárolt attríbútum false, tehát van tárolt szám,
            // visszaadjuk a tárolt számot és true-ra állítjuk a nincsTárolt változót
            nincsTarolt = !nincsTarolt;
            return tarolt;
        }
    }

    public static void main(String[] args) {
        // létrehozunk egy objektumot ami a Polargen osztálytól örökli a tulajdonságait
        Polargen pg = new Polargen();

        // legyártunk 10 számot a kovetkezo függvény matekja szerint
        for (int i = 0; i < 10; i++) {
            System.out.println(pg.kovetkezo());
        }
    }
}