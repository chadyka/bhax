#include <iostream>
#include <ios>
#include <vector>
#include <sstream>

void readints (std::vector<int>& s)
{
    std::ios_base::iostate old_state = std::cin.exceptions();
    std::cin.exceptions(std::ios_base::eofbit);

    std::string str;
    for ( ;; )
    {
        try
        {
            std::cin >> str;
            int i;
            std::stringstream(str) >> i;
            std::cout << "beolv: " << i << std::endl;
            s.push_back(i);
        }
        catch(std::ios_base::failure f)
        {
            std::cout << "exception" << std::endl;
            break;
        }
        catch (...)
        {
            std::cout << "..." << std::endl;
            break;
        }

    }
    std::cin.exceptions(old_state);

}

int main() {
    std::vector<int> v;

    readints(v);

    for (auto i : v)
    {
        std::cout << i << ", ";
    }
    std::cout << std::endl;
    return 0;
}