#include <iostream>

int main() {
    int *pa;
    int (*pb)[5];
    int *pc;
    int a, b[5];
    pa = &a;
    pb = &b;
    pc = b;

    printf("pa    = %p | pb    = %p | pc   = %p\n", pa, pb, pc);

    ++pa;
    ++pb;
    ++pc;

    printf("++pa  = %p | ++pb  = %p | ++pc = %p\n", pa, pb, pc);

    int& ref_a = a;
    int (&ref_b)[5] = b;

    printf("ref_a = %p | ref_b = %p\n",&ref_a,&ref_b);

    return 0;
}